open Astring

module Id = struct
  let base26_2_from_int i =
    let l = 26 in
    assert (i >= 0);
    assert (i < 676);
    assert (l * l = 676);
    assert (97 = ('a' |> Char.to_int));
    let c0 = (i mod l) + 97 |> Char.of_int |> Option.get in
    let i = i / l in
    let c1 = (i mod l) + 97 |> Char.of_int |> Option.get in
    String.v ~len: 2 (function
        | 1 -> c0
        | 0 -> c1
        | _ -> failwith __LOC__)

  let make
      ~(invent : int -> string)
      ~prefix
      ?(max_tries = 100)
      exists =
    let rec mk tries_left =
      let id = prefix ^ (max_tries - tries_left |> invent) in
      if not (id |> exists)
      then id
      else if tries_left <= 0
      then failwith __LOC__
      else mk (tries_left |> pred)
    in
    mk max_tries

  (* https://github.com/aantron/lambdasoup/issues/59
     and https://digitalcourage.social/@mro/112891719464884054 *)
  let to_css =
    let r = Str.regexp_string ":" in
    Str.global_replace r {|\\\0|}

  let rx = Str.regexp {|^[ ]*#\(fn\(:\([^ ]*\)\)?\)[ ]*$|}

  let from_href ~invent ~prefix ~exists hre =
    if Str.string_match rx hre 0
    then (
      match try Str.matched_group 3 hre 
        with Not_found -> "" with
      | "" -> make ~invent ~prefix exists
      | id -> prefix ^ id
    ) else
      failwith (Printf.sprintf "%s: '%s'" __LOC__ hre) 
end

let uniq_c_rev equ lst =
  let rec f init = function
    | [] -> init
    | hd :: tl ->
      let equ' = equ hd in
      let init = (hd,1 + (tl |> List.filter equ' |> List.length)) :: init in
      tl
      |> List.filter (fun v -> not (equ' v))
      |> f init
  in
  f [] lst

let expand
    ?(invent = fun _ _ -> 676 |> Random.int |> Id.base26_2_from_int)
    soup : 'a Soup.node =
  assert (26 * 26 = 676);
  let open Soup in
  let seq = ref 0 in
  let lst = ref [] in
  let equ (a,_) (b,_) = (* compare ids, ignore labels *) String.equal a b in
  let prefix = "fn:" in
  let exists id = soup |> select_one (Id.to_css ("#" ^ id)) |> Option.is_some in
  let updat (id_,(lbl,ref_)) a =
    let lbl = lbl |> succ |> string_of_int in
    let open Soup in
    a |> clear;
    "[" ^ lbl ^ "]" |> create_text |> append_child a;
    a |> set_attribute "href" ("#" ^ id_);
    a |> set_attribute "id" ("ref:" ^ (ref_ |> string_of_int) ^ ":" ^ id_);
    a |> delete_attribute "name" in
  soup |> select "a[href^=#fn]" |> iter (fun a ->
      let invent = !lst |> List.length |> invent in
      let id = a |> R.attribute "href" |> Id.from_href ~invent ~prefix ~exists in
      let (lbl,ref_),step = match !lst |> List.assoc_opt id with
        | None       -> (!seq,0),1
        | Some (l,r) -> (l, (r |> succ)),0 in
      lst := (id,(lbl,ref_)) :: !lst;
      seq := !seq + step;
      a |> updat (id,(lbl,ref_)) );
  let ol = match select_one {|ol[data-footnotes-generator=mro.name/pagerake]|} soup with
    | Some e -> e
    | None ->
      let e = "ol" |> create_element ~attributes:[
          "data-footnotes-generator","mro.name/pagerake";
          (* "role","doc-endnotes" https://www.w3.org/TR/dpub-aria-1.1/#doc-endnotes *)] ~inner_text:"\n" in
      "\n" |> create_text |> append_root soup;
      e |> append_root soup;
      e in
  !lst
  |> List.rev
  |> uniq_c_rev equ
  |> List.iter (fun ((id,_lbl),n) ->
      (* ensure id is part of ol *)
      let li = match ol |> select_one (Id.to_css ("#" ^ id)) with
        | Some li ->
          assert (li |> name |> String.equal "li");
          li |> delete_attribute "name";
          li |> select "span > a[href^=#ref:]" |> iter (fun a -> a |> R.parent |> delete);
          li
        | None ->
          "li" |> create_element ~id ~inner_text:"t.b.d." in
      assert (li |> name |> String.equal "li");
      li |> prepend_child ol;
      "\n" |> create_text |> prepend_child ol;
      for rn = 0 to (n - 1) do
        let hre = "#ref:" ^ (rn |> string_of_int) ^ ":" ^ id in
        match li |> select_one ("a[href=" ^ hre ^ "]") with
        | Some _ -> ()
        | None ->
          let spa = "span" |> create_element in
          ", " |> create_text |> append_child spa;
          "a" |> create_element ~attributes:["href",hre] ~inner_text:"back^" |> append_child spa;
          spa |> append_child li;
      done
    );
  soup

let collapse
    soup : 'a Soup.node =
  let open Soup in
  soup |> select {|a[href^=#fn:]|} |> iter (fun a ->
      a |> clear;
      a |> delete_attribute "id";
      a |> delete_attribute "name";
      "[-]" |> create_text |> append_child a);
  soup |> select {|ol[data-footnotes-generator=mro.name/pagerake] > li[id^=fn:] > span > a[href^=#ref:]|} |> iter (fun a ->
      let span = a |> R.parent in
      assert (span |> name |> String.equal "span");
      span |> delete;
      ());
  soup |> select {|ol[data-footnotes-generator=mro.name/pagerake] > li[id^=fn:]|} |> iter (delete_attribute "name");
  soup
