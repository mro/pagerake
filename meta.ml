
let multi fkt lst =
  lst |>
  List.fold_left
    (fun init fn -> fn |> fkt |> max init)
    0

let fields = [
  "terms-of-service";
  "privacy";
  "license";
  "lang";
  "author";
  "keywords";
  "title";
]

let print_fields ?(warn = "⚠️") lst =
  fields
  |> List.fold_left (fun init k ->
      match lst |> List.assoc_opt k with
      | None ->
        Printf.fprintf stdout "%s: %s\n" k warn;
        1
      | Some v ->
        Printf.fprintf stdout "%s: %s\n" k v;
        init)
    0

let extract soup : (string * string) list =
  let open Soup in
  let attr lst k css att =
    match soup |> select_one css with
    | Some n -> (match n |> attribute att with
        | None -> lst
        | Some v' -> (k,v') :: lst)
    | None -> lst in
  let a lst k css = attr lst k css "href" in
  let meta lst k css = attr lst k css "content" in
  let lang lst k css = attr lst k css "lang" in
  let title lst k css =
    match soup |> select_one css with
    | Some v -> (k,v |> texts |> String.concat "") :: lst
    | None -> lst in
  let r = [] in
  let r = a     r "terms-of-service" "html > body a[rel*=terms-of-service]" in
  let r = a     r "privacy"          "html > body a[rel*=privacy]" in
  let r = a     r "license"          "html > body a[rel*=license]" in
  let r = lang  r "lang"             "html[lang]" in
  let r = meta  r "author"           "html > head > meta[name=author]" in
  let r = meta  r "keywords"         "html > head > meta[name=keywords]" in
  let r = title r "title"            "html > head > title" in
  r

let extract1 fn =
  let open Soup in
  match fn
        |> read_file
        |> parse
        |> extract
        |> List.length == (fields |> List.length)
  with
  | true  ->
    fn |> print_endline;
    0
  | false ->
    fn |> prerr_endline;
    1
