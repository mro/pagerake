
let exec args =
  let print_version oc =
    let exe = Filename.basename Sys.executable_name in
    Printf.fprintf oc "%s: https://mro.name/prake/v/%s+%s\n" exe Version.dune_project_version Version.git_sha;
    0
  and print_help oc =
    let _exe = Filename.basename Sys.executable_name in
    Printf.fprintf oc
      {|Some HTML helpers.

OPTIONS

  --help, -h
      print this help

  --version, -V
      print version

COMMANDS

  footnotes expand < stdin > stdout
    replace
      <a href="#fn"></a>
    with
      <a href="#fn:xy" id="ref:0:fn:xy">[1]</a>
    and ensure according <li id="fn:xy"> in <ol data-footnotes-generator="mro.name/pagerake"></ol>
    in ascending order.
  footnotes collapse < stdin > stdout
    remove redundant stuff introduced by footnotes expand.

  ha expand < stdin > stdout
    add <a href> child nodes to all <h?> with id
  ha collapse < stdin > stdout
    remove redundant stuff introduced by ha expand.

  meta extract < stdin
    some meta data from a webpage.
  meta check fn0 fn1 fn2 ...
    operate on multiple files. Write ok to stdout, err to stderr.
|};
    0
  in
  match args with
  | [ _; "-h" ] | [ _; "--help" ] -> print_help stdout
  | [ _; "-V" ] | [ _; "--version" ] -> print_version stdout
  | [ _; "footnotes"; "expand" ] -> (
      stdin
      |> Soup.read_channel
      |> Soup.parse
      |> Footnote.expand
      |> Soup.to_string
      |> Soup.write_channel stdout;
      0)
  | [ _; "footnotes"; "collapse" ] -> (
      stdin
      |> Soup.read_channel
      |> Soup.parse
      |> Footnote.collapse
      |> Soup.to_string
      |> Soup.write_channel stdout;
      0)
  | [ _; "ha"; "expand" ] -> (
      stdin
      |> Soup.read_channel
      |> Soup.parse
      |> Ha.expand
      |> Soup.to_string
      |> Soup.write_channel stdout;
      0)
  | [ _; "ha"; "collapse" ] -> (
      stdin
      |> Soup.read_channel
      |> Soup.parse
      |> Ha.collapse
      |> Soup.to_string
      |> Soup.write_channel stdout;
      0)
  | [ _; "meta"; "extract" ] -> (
      stdin
      |> Soup.read_channel
      |> Soup.parse
      |> Meta.extract
      |> Meta.print_fields)
  |  _ :: "meta" :: "check" :: l ->
    l |> Meta.(multi extract1)
  | _ ->
    prerr_endline "get help with -h";
    1

let () =
  Sys.argv
  |> Array.to_list
  |> exec
  |> exit
