#!/bin/sh
#
# Copyright (C) 2021, https://mro.name/pagerake

lst_file_xpath () {
  local v
  v="$(xmllint --html --xpath "string(${2})" "${1}" 2>/dev/null)"
  if [ "_" = "_${v}" ] ; then
    echo "<missing>"
    return 1
  else
    echo "${v}"
    return 0
  fi
}

lst_file () {
  local f="${1}"
  for xp in \
    '/html/body//a[@rel="license"]/@href' \
    '/html/body//a[@rel="privacy"]/@href' \
    '/html/body//a[@rel="terms-of-service"]/@href' \
    '/html/@lang' \
    '/html/head/meta[@name="author"]/@content' \
    '/html/head/meta[@name="keywords"]/@content' \
  ; do
    local v
    v="$(lst_file_xpath "${f}" "${xp}")"
    echo "${f}: ${xp} = ${v}"
  done
}


[ "${1}" = "lst" ] && {
  shift
  while [ "_" != "_${1}" ]
  do
    lst_file "${1}"
    shift
  done
  exit 0
}

[ "${1}" = "-V" ] && {
  echo "${0} https://semestriel.framapad.org/p/pagerake-html-helper-9mk9"
  exit 0
}

[ "${1}" = "-h" ] && {
  cat <<EOF

Synposis

 \$ ${0} -h       # this help
 \$ ${0} -V       # version
 \$ ${0} lst index.html foo.html *.html

EOF
  exit 0
}

exit 1
