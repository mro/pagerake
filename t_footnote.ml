open Alcotest

let tc_rx () =
  let _rx = Str.regexp {|[ ]*#fn:\([^ ]*\)[ ]*$|} in
  let rx = Footnote.Id.rx in
  let hre = "#fn:ap_actor" in
  let b = Str.string_match _rx hre 0 in
  assert b;
  Str.matched_group 1 hre |> check string __LOC__ "ap_actor";
  ();
  let hre = "#fn:ap_actor" in
  let b = Str.string_match rx hre 0 in
  assert b;
  Str.matched_group 3 hre |> check string __LOC__ "ap_actor";
  ();
  let hre = "#fn:" in
  let b = Str.string_match rx hre 0 in
  assert b;
  Str.matched_group 3 hre |> check string __LOC__ "";
  ();
  let hre = "#fn" in
  let b = Str.string_match rx hre 0 in
  assert b;
  (try
     Str.matched_group 3 hre |> check string __LOC__ "";
     failwith __LOC__
   with Not_found -> ());
  ()

let tc_to_css () =
  "fn:2"
  |> Footnote.Id.to_css
  |> check string __LOC__ {|fn\:2|}

let tc_uniq_c () =
  let l = ["a" ; "b"; "a"; "c"; "c"; "a"; "d"] in
  match l |> Footnote.uniq_c_rev String.equal |> List.rev with
  | ["a",3 ; "b",1 ; "c",2 ; "d",1] -> ()
  | _ -> failwith __LOC__

let tc_i2a () =
  0 |> Footnote.Id.base26_2_from_int |> check string __LOC__ "aa";
  28 |> Footnote.Id.base26_2_from_int |> check string __LOC__ "bc";
  ()

let tc_expand () =
  let invent fn retry = fn * 20 + retry |> Footnote.Id.base26_2_from_int in
  {|prefix <a href="#fn:"></a> more <a href="#fn:"></a>.|} |> Soup.parse
  |> Footnote.expand ~invent |> Soup.to_string |> check string __LOC__
    {|prefix <a id="ref:0:fn:aa" href="#fn:aa">[1]</a> more <a id="ref:0:fn:au" href="#fn:au">[2]</a>.
<ol data-footnotes-generator="mro.name/pagerake">
<li id="fn:aa">t.b.d.<span>, <a href="#ref:0:fn:aa">back^</a></span></li>
<li id="fn:au">t.b.d.<span>, <a href="#ref:0:fn:au">back^</a></span></li>
</ol>|};
  (* *)
  {|prefix <a href="#fn:"></a>
more <a href="#fn:">[a]</a>
even more <a href="#fn:"></a>.|} |> Soup.parse
  |> Footnote.expand ~invent |> Soup.to_string |> check string __LOC__
    {|prefix <a id="ref:0:fn:aa" href="#fn:aa">[1]</a>
more <a id="ref:0:fn:au" href="#fn:au">[2]</a>
even more <a id="ref:0:fn:bo" href="#fn:bo">[3]</a>.
<ol data-footnotes-generator="mro.name/pagerake">
<li id="fn:aa">t.b.d.<span>, <a href="#ref:0:fn:aa">back^</a></span></li>
<li id="fn:au">t.b.d.<span>, <a href="#ref:0:fn:au">back^</a></span></li>
<li id="fn:bo">t.b.d.<span>, <a href="#ref:0:fn:bo">back^</a></span></li>
</ol>|};
  (* *)
  {|prefix <a href="#fn:"></a>
more <a href="#fn:au"></a>
<ol data-footnotes-generator="mro.name/pagerake">
<li id="fn:au">here!<span>, <a href="#ref:0:fn:au">back^</a></span></li>
</ol>
done!|} |> Soup.parse
  |> Footnote.expand ~invent |> Soup.to_string |> check string __LOC__
    {|prefix <a id="ref:0:fn:aa" href="#fn:aa">[1]</a>
more <a id="ref:0:fn:au" href="#fn:au">[2]</a>
<ol data-footnotes-generator="mro.name/pagerake">
<li id="fn:aa">t.b.d.<span>, <a href="#ref:0:fn:aa">back^</a></span></li>
<li id="fn:au">here!<span>, <a href="#ref:0:fn:au">back^</a></span></li>

</ol>
done!|};
  ()

let tc_collapse () =
  {|prefix <a id="ref:0:fn:aa" href="#fn:aa">[1]</a>
more <a id="ref:0:fn:au" href="#fn:au">[2]</a>
<ol data-footnotes-generator="mro.name/pagerake">
<li id="fn:aa">t.b.d.<span>, <a href="#ref:0:fn:aa">back^</a></span></li>
<li id="fn:au">t.b.d.<span>, <a href="#ref:0:fn:au">back^</a></span></li>
</ol>
done!|}
  |> Soup.parse
  |> Footnote.collapse
  |> Soup.to_string
  |> check string __LOC__ {|prefix <a href="#fn:aa">[-]</a>
more <a href="#fn:au">[-]</a>
<ol data-footnotes-generator="mro.name/pagerake">
<li id="fn:aa">t.b.d.</li>
<li id="fn:au">t.b.d.</li>
</ol>
done!|};
  ()

let () =
  Printexc.record_backtrace true;
  run
    "Footnote" [
    __FILE__ , [
      "tc_rx "      , `Quick, tc_rx;
      "tc_to_css "  , `Quick, tc_to_css;
      "tc_uniq_c "  , `Quick, tc_uniq_c;
      "tc_i2a "     , `Quick, tc_i2a;
      "tc_expand "  , `Quick, tc_expand;
      "tc_collapse ", `Quick, tc_collapse;
    ]
  ]
