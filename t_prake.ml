open Alcotest

let test_1st () =
  let rx = Str.regexp {|^ *\[\([0-9]+\)\] *$|} in
  let txt = "[2]" in
  assert (Str.string_match rx txt 0);
  Str.matched_group 1 txt |> check string __LOC__ "2";
  ()

let test_soup_append () =
  {|prefix <ol></ol> suffix|} |> Soup.parse
  |> Soup.to_string |> check string __LOC__
    {|prefix <ol></ol> suffix|};
  (* *)
  let so = {|prefix
|} |> Soup.parse in
  let e = Soup.create_element "ol" in
  Soup.append_root so e;
  so |> Soup.to_string |> check string __LOC__
    {|prefix
<ol></ol>|};
  (* *)
  ()

let () =
  run
    "Prake" [
    __FILE__ , [
      "tc_1st", `Quick, test_1st;
      "tc_soup_append", `Quick, test_soup_append;
    ]
  ]
