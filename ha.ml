
let expand
    soup : 'a Soup.node =
  let open Soup in
  soup |> select "*[id]" |> iter (fun h ->
      match h |> name with
      | "h1"
      | "h2"
      | "h3"
      | "h4" -> (
          let href= "#" ^ (h |> R.attribute "id") in
          match h |> select_one ("a[href='" ^ href ^ "']") with
          | None -> let a = "a" |> create_element ~attributes:["href",href] in
            h
            |> children
            |> iter (append_child a);
            a |> append_child h 
          | Some _ -> () )
      | _ -> ());
  soup

let collapse
    soup : 'a Soup.node =
  let open Soup in
  soup |> select "a[href]" |> iter (fun a ->
      match a |> parent with
      | Some h ->
        (match h |> name with
         | "h1"
         | "h2"
         | "h3"
         | "h4" -> (
             a |> delete;
             a
             |> children
             |> iter (append_child h);
             h |> delete_attribute "name")
         | _ -> () )
      | None -> ());
  soup
